'use strict';

const mongodb = require('mongodb');
const http = require('http');

// Imports the Google Cloud client library
const PubSub = require('@google-cloud/pubsub');


// Your Google Cloud Platform project ID
const projectId = 'veuhub-185502';

// Instantiates a client
const pubsub = PubSub({
  projectId: projectId,
  keyFilename:'veuhub-c9421aa9de0f.json'
});

// Reference a topic that has been previously created.
var topic = pubsub.topic('adtech');


let uri = `mongodb:///root@d2DAkCBxgzz2:/opt/bitnami/mongodb/tmp/mongodb-27017.sock/rtblog`;
// let uri = `mongodb://localhost:27017/test`;
let dbname = `rtblog`;


mongodb.MongoClient.connect(uri, (err, db) => {	
  if (err) {
    throw err;
  }
  

 
var subscription = topic.subscription('adtechsub');

subscription.on('message', function(message) {




	var type=message.attributes;
   

	var data =JSON.parse(message.data.toString('utf8'));
 
	if(type.type=="ssp_request"){
			// check if site named parameter exists on data 
			if(data.site){
		        var text = '{ "'+data.SSPID+'" : [' +
								'{ "url":"'+data.site.domain+'" , "verification_flag":3 }' +
							']}';
				var obj = JSON.parse(text);
		  		var search = data.SSPID;
				var find = {};
				find[search] = { $exists: true, $ne: null };
				db.collection("ssp_urls").find(find).toArray(function(err, result) {
				    if (err) throw err;
				    if(result != ""){
				    	//update the url to current ssp
				    	var find_new = {};
							find_new[search] =  { $elemMatch: { url: data.site.domain } } ;
							console.log(find_new);
					    //check if url already added or not
				    	db.collection("ssp_urls").find(find_new).toArray(function(err, result_new){
				    		
				    		if(result_new =="" && result[0]._id){
				    			var text_new = '{ "url":"'+data.site.domain+'" , "verification_flag":3 }' ;
							    var obj_new = JSON.parse(text_new);
						    	var myquery = { _id: result[0]._id };
						    	var obj1 = {};
			      					obj1[search] = obj_new;
							  	var newvalues = {$push: obj1 };
							  	db.collection("ssp_urls").update(myquery, newvalues, function(err, res) {
							    	if (err) throw err;
							    	console.log("1 document updated");
							    	db.close();
							  	});
				    		}
				    		console.log("1 document checked");
				    	});
				        
				    	
				    }
				    else {
				    	//insert new ssp
				    	db.collection("ssp_urls").insert(obj, function(err, res) {
							if (err) throw err;
							console.log("1 document ssp_request inserted");
						
						  });

				    }
				   
				 });

			}


			db.collection("ssp_request").insertOne(data, function(err, res) {
				if (err) throw err;
				console.log("1 document ssp_request inserted");
			
			  });
		}
		else if(type.type=="rejected_ssp_request"){
		 db.collection("rejected_ssp_request").insertOne(data, function(err, res) {
			if (err) throw err;
			console.log("1 document rejected_ssp_request inserted");
		
		  });
		}else if(type.type=="nobid_dsp_response"){
		 db.collection("nobid_dsp_response").insertOne(data, function(err, res) {
			if (err) throw err;
			console.log("1 document nobid_dsp_response inserted");
		
		  });
		}else if(type.type=="valid_dsp_response"){
		 db.collection("valid_dsp_response").insertOne(data, function(err, res) {
			if (err) throw err;
			console.log("1 document valid_dsp_response inserted");
		
		  });
		}

        
		message.ack();  
 	});
	

	});
